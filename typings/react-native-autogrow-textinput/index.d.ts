declare module 'react-native-autogrow-textinput' {
  import { TextInputProps } from "react-native";

  export interface AutoGrowingTextInputProps extends TextInputProps {
  }

  export class AutoGrowingTextInput extends React.Component<AutoGrowingTextInputProps> {}
}