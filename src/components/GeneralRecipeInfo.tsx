import React from 'react';
import { Text, View, TextInput, Picker } from 'react-native';
import CommonStyles from '../shared/CommonStyles';
import { Category } from '../shared/DataTypes';
import { categories } from '../shared/Categories';

export interface GeneralRecipeInfoProps {
  categoryId?: number;
}

interface State {
  categories: Category[];
  title?: string;
  categoryId?: number;
  amount?: string;
  complexity?: number;
  time?: string;
}

export class GeneralRecipeInfo extends React.Component<GeneralRecipeInfoProps, State> {
  constructor(props: GeneralRecipeInfoProps) {
    super(props);

    this.state = {
      categories
    };
  }

  render() {
    return (
      <View style={CommonStyles.formContainer}>
        <Text style={CommonStyles.formControlLabel}>Title</Text>
        <TextInput
          style={CommonStyles.formControl}
          value={this.state.title}
          onChangeText={(title: string) => this.setState({title})}/>
        <Text style={CommonStyles.formControlLabel}>Category</Text>
        <Picker 
          selectedValue={this.state.categoryId}
          onValueChange={(itemValue, _itemIndex: number) => this.setState({categoryId: itemValue})}>
          {this.state.categories.map((cat: Category) => 
            <Picker.Item label={cat.title} value={cat.id} key={cat.id} />)}
        </Picker>
        <Text style={CommonStyles.formControlLabel}>Amount</Text>
        <TextInput
          style={CommonStyles.formControl}
          value={this.state.amount}
          onChangeText={(amount: string) => this.setState({amount})}/>
        <Text style={CommonStyles.formControlLabel}>Complexity</Text>
        <Picker
          selectedValue={this.state.complexity}
          onValueChange={(itemValue) => this.setState({complexity: itemValue})}>
          {[1, 2, 3, 4, 5].map((i: number) => 
            <Picker.Item value={i} key={i} label={i.toString()}/>)}
        </Picker>
        <Text style={CommonStyles.formControlLabel}>Time Required</Text>
        <TextInput
          style={CommonStyles.formControl}
          value={this.state.time}
          onChangeText={(time: string) => this.setState({time})}/>
      </View>
    );
  }
}