import React from 'react';
import { Step } from '../shared/DataTypes';
import { View, Text, TextInput } from 'react-native';
import CommonStyles from '../shared/CommonStyles';
import { ColorScheme } from '../shared/ColorScheme';
import { Button } from 'react-native-elements';
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';

export interface AddStepProps {
  onCancel: () => any;
  onAdd: (step: Step) => any;
}

interface State {
  description?: string;
  time?: string;
}

export class AddStep extends React.Component<AddStepProps, State> {
  render() {
    return (
      <View style={CommonStyles.formContainer}>
        <Text style={CommonStyles.formControlLabel}>Description</Text>
        {/* <TextInput
          onChangeText={(description: string) => this.setState({description})}
          numberOfLines={7}
          multiline={true}
          style={[CommonStyles.formControl, {height: 200}]}
          /> */}
        <AutoGrowingTextInput
          onChangeText={(description: string) => this.setState({description})}
          style={CommonStyles.formControl}
          />
        <Text style={CommonStyles.formControlLabel}>Time</Text>
        <TextInput
          onChangeText={(time: string) => this.setState({time})}
          style={CommonStyles.formControl}
          />

        <View style={{flexDirection: 'row'}}>
          <Button
            title="Cancel"
            onPress={() => this.props.onCancel()}
            containerViewStyle={{ flex: 1 }}
            backgroundColor={ColorScheme.c1}
            borderRadius={5}
          />
          <Button
            title="Add"
            onPress={() => this.handleAdd()}
            containerViewStyle={{ flex: 1 }}
            backgroundColor={ColorScheme.c1}
            borderRadius={5}
          />
        </View>
      </View>
    );
  }

  handleAdd() {
    const step: Step = {
      description: this.state.description,
      time: this.state.time
    };

    this.props.onAdd(step);
  }
}