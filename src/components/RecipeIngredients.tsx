import React from 'react';
import { FlatList, View, Modal, Text, Alert } from 'react-native';
import { List, ListItem, Icon } from 'react-native-elements';
import { IngredientRef } from '../shared/DataTypes';
import { FlatListRendererArguments } from '../shared/HelperTypes';
import ListBasedStyles from '../shared/ListBasedStyles';
import { NavigationScreenProps } from 'react-navigation';
import { AddIngredient } from './AddIngredient';
import { findIngredientById } from '../shared/Ingredients';
import { ColorScheme } from '../shared/ColorScheme';

interface State {
  ingredients: IngredientRef[];
  addIngredientModalShown: boolean;
}
export class RecipeIngredients extends React.Component<NavigationScreenProps, State> {
  constructor(props: NavigationScreenProps) {
    super(props);

    this.state = {
      ingredients: [],
      addIngredientModalShown: false
    };

    this.addIngredient = this.addIngredient.bind(this);
  }

  render() {
    return (
      <View style={ListBasedStyles.container}>
        <List containerStyle={ListBasedStyles.list}>
          <FlatList
            data={this.state.ingredients}
            renderItem={(item: Element) => this.renderIngredientRef(item as FlatListRendererArguments<IngredientRef>)}
            keyExtractor={(item: Element) => (item as IngredientRef).ingredientId.toString()}
            />
        </List>
        <Icon
          raised
          reverse
          name="plus"
          type="font-awesome"
          onPress={() => this.setIngredientModalVisibility(true)}
          color={ColorScheme.c1}
          containerStyle={{alignSelf: 'flex-end'}}/>

        <Modal
          animationType="slide"
          visible={this.state.addIngredientModalShown}
          onRequestClose={() => this.setIngredientModalVisibility(false)}>
          <AddIngredient 
            existingIngredients={new Set(this.state.ingredients.map((i: IngredientRef) => i.ingredientId))}
            onAdd={this.addIngredient}
            onCancel={() => this.setIngredientModalVisibility(false)}/>
        </Modal>
      </View>
    );
  }

  renderIngredientRef({item, index}: FlatListRendererArguments<IngredientRef>): JSX.Element {
    const ingredient = findIngredientById(item.ingredientId);
    if (!ingredient) {
      return (<Text>Element not found!</Text>);
    }
    return (
      <ListItem
        title={ingredient.title}
        subtitle={item.amount}
        onLongPress={() => this.removeIngredient(index)}
        key={index}
        hideChevron
        underlayColor={ColorScheme.c2}
        titleStyle={{color: ColorScheme.c1, fontSize: 18}}
        containerStyle={{borderBottomColor: ColorScheme.c1}}/>
    )
  }

  setIngredientModalVisibility(b: boolean) {
    this.setState({addIngredientModalShown: b});
  }

  addIngredient(i: IngredientRef) {
    this.setState({
      ingredients: this.state.ingredients.concat([i]),
      addIngredientModalShown: false
    });
  }

  removeIngredient(index: number) {
    const ingredient = findIngredientById(this.state.ingredients[index].ingredientId);
    if (!ingredient) {
      return;
    }
    Alert.alert(
      'Are you sure?',
      `Remove ${ingredient.title} from the list?`,
      [
        {
          text: 'Cancel'
        },
        {
          text: 'Yes',
          onPress: () => this.setState({
            ingredients: this.state.ingredients.filter((_v: IngredientRef, idx: number) => idx !== index)
          })
        }
      ]
    )
  }
}