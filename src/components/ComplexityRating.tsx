import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

export interface ComplexityRatingProps {
  complexity?: number;
  total: number;
}

export class ComplexityRating extends React.Component<ComplexityRatingProps> {
  
  render(): JSX.Element {
    const image1 = require('../../assets/complexity_full.jpg');
    const image2 = require('../../assets/complexity.jpg');
    const content: JSX.Element[] = new Array<JSX.Element>(this.props.total);
    let c = this.props.complexity || 0;
    for (let i = 0; i < this.props.total; i++) {
      const src = i < c ? image1 : image2;
      content[i] = <Image source={src} key={i} style={styles.image}/>;
    }
    return (
      <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
        {content}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: 20,
    height: 20
  }
});