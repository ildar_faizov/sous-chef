import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Icon } from 'react-native-elements';
import { ColorScheme } from '../shared/ColorScheme';
import { NavigationScreenProps } from 'react-navigation';

const styles = StyleSheet.create({
  panel: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});

export interface ButtonPanelProps extends NavigationScreenProps {

}

export class ButtonPanel extends React.Component<ButtonPanelProps> {
  render() {
    return (
      <View style={styles.panel}>
        <Icon
          raised
          reverse
          name="search"
          type="font-awesome"
          color={ColorScheme.c1}
          onPress={() => this.onSearch() }/>
        <Icon
          raised
          reverse
          name="plus"
          type="font-awesome"
          color={ColorScheme.c1}
          onPress={() => this.onAddRecipe()}/>
      </View>
    );
  }

  onSearch() {
    console.log('search');
  }

  onAddRecipe() {
    this.props.navigation.navigate('NewRecipe');
  }

}