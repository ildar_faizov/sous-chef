import React from 'react';
import { FlatList, View } from 'react-native';
import { List, ListItem, Avatar } from 'react-native-elements';
import { categories } from '../shared/Categories';
import { Category } from '../shared/DataTypes';
import { FlatListRendererArguments } from '../shared/HelperTypes';
import { ButtonPanel } from './ButtonPanel';
import { ColorScheme } from '../shared/ColorScheme';
import ListBasedStyles from '../shared/ListBasedStyles';
import { NavigationScreenProps } from 'react-navigation';

interface State {
  categories: Category[];
}

export class Categories extends React.Component<NavigationScreenProps, State> {
  constructor(props: any) {
    super(props);

    this.state = {
      categories
    };
  }
  
  render() {
    return (
      <View style={ListBasedStyles.container}>
        <List containerStyle={ListBasedStyles.list}>
          <FlatList
            data={categories}
            keyExtractor={(item: Element) => (item as Category).id.toString()}
            renderItem={(item: Element) => this.renderCategory(item as FlatListRendererArguments<Category>)}
            />
        </List>
        <ButtonPanel navigation={this.props.navigation}/>
      </View>
    );
  }

  renderCategory({item, index}: FlatListRendererArguments<Category>) : JSX.Element {
    return (
      <ListItem 
        key={index}
        title={item.title}
        onPress={() => this.onCategoryPress(item)}
        chevronColor={ColorScheme.c1}
        underlayColor={ColorScheme.c2}
        titleStyle={{color: ColorScheme.c1, fontSize: 22}}
        containerStyle={{borderBottomColor: ColorScheme.c1}}
        avatar={<Avatar source={item.image} large />}
        />
    );
  }

  onCategoryPress(category: Category) {
    this.props.navigation.navigate('Recipes', {categoryId: category.id});
  }
}