import React from 'react';
import { FlatList, View, Text, StyleSheet } from 'react-native';
import { List, ListItem } from 'react-native-elements';
import { Recipe } from '../shared/DataTypes';
import { FlatListRendererArguments } from '../shared/HelperTypes';
import { recipes } from '../shared/Recipes';
import { ButtonPanel } from './ButtonPanel';
import ListBasedStyles from '../shared/ListBasedStyles';
import { ColorScheme } from '../shared/ColorScheme';
import { ComplexityRating } from './ComplexityRating';
import { NavigationScreenProps } from 'react-navigation';

interface State {
  categoryId?: number;
  recipes: Recipe[];
}

export class Recipes extends React.Component<NavigationScreenProps, State> {
  constructor(props: any) {
    super(props);

    this.state = {
      recipes: []
    };
  }

  componentDidMount() {
    this.componentDidUpdate(null, null);
  }

  componentDidUpdate(_prevProps: any, prevState?: State) {
    const { navigation } = this.props;
    const categoryId = navigation.getParam('categoryId');
    if (this.state.categoryId !== categoryId) {
      this.setState({categoryId});
    }
    if (prevState && this.state.categoryId !== prevState.categoryId) {
      this.fetchRecipes(categoryId)
        .then((recipes: Recipe[]) => this.setState({recipes}));
    }
  }

  fetchRecipes(categoryId: number) : Promise<Recipe[]> {
    const res = (recipes as Recipe[]).filter((r : Recipe) => r.categoryId === categoryId);
    return Promise.resolve(res);
  }

  render() : JSX.Element {
    return (
      <View style={ListBasedStyles.container}>
        <List containerStyle={ListBasedStyles.list}>
          <FlatList
            data={this.state.recipes}
            renderItem={(item: Element) => this.renderRecipe(item as FlatListRendererArguments<Recipe>)}
            keyExtractor={(item: Element) => (item as Recipe).id.toString()}
            />
        </List>
        <ButtonPanel navigation={this.props.navigation}/>
      </View>
    );  
  }

  renderRecipe({item, index} : FlatListRendererArguments<Recipe>) : JSX.Element {

    const content: JSX.Element = 
      <View style={styles.content}>
        <Text style={{marginRight: 5}}>Complexity:</Text>
        <ComplexityRating complexity={item.complexity} total={5} />
        <View style={{flexGrow: 1, flexShrink: 1, flexBasis: 0}}></View>
        <Text>{item.time}</Text>
      </View>;

    return (
      <ListItem 
        key={index}
        title={item.title}
        subtitle={content}
        hideChevron
        underlayColor={ColorScheme.c2}
        titleStyle={{color: ColorScheme.c1, fontSize: 22}}
        containerStyle={{borderBottomColor: ColorScheme.c1}}/>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1, 
    flexDirection: 'row', 
    justifyContent: 'flex-start',
    paddingLeft: 10
  }
});