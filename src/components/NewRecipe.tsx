import React from 'react';
import { createMaterialTopTabNavigator, NavigationScreenProps } from 'react-navigation';
import { GeneralRecipeInfo } from './GeneralRecipeInfo';
import { RecipeIngredients } from './RecipeIngredients';
import { RecipeSteps } from './RecipeSteps';
import { View } from 'react-native';
import { Button } from 'react-native-elements';
import { ColorScheme } from '../shared/ColorScheme';

const TabNavigator = createMaterialTopTabNavigator({
  General: GeneralRecipeInfo,
  Ingredients: RecipeIngredients,
  Steps: RecipeSteps
}, {
  tabBarOptions: {
    style: {
      backgroundColor: ColorScheme.c1
    }
  }
});

export class NewRecipe extends React.Component<NavigationScreenProps> {
  render() {
    return (
      <View style={{flex: 1, backgroundColor: ColorScheme.c3}}>
        <TabNavigator style={{flex: 1}}/>
        <View style={{flex: 0, flexDirection: 'row', height: 50, marginBottom: 5}}>
          <Button
            title="Cancel"
            onPress={() => this.onCancel()}
            containerViewStyle={{flex: 1}}
            backgroundColor={ColorScheme.c1}
            borderRadius={5}
            />
          <Button
            title="Save"
            icon={{name: 'save', type: 'font-awesome'}}
            onPress={() => this.onSave()}
            containerViewStyle={{flex: 1}}
            backgroundColor={ColorScheme.c1}
            borderRadius={5}
            />
        </View>
      </View>
    );
  }

  onCancel() {
    this.props.navigation.goBack();
  }

  onSave() {
    // TODO:
  }
}