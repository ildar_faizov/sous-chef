import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    flexDirection: 'row', 
    alignItems: 'center', 
    paddingLeft: 10
  },
  title: {
    color: '#fff', 
    fontSize: 24, 
    fontWeight: 'bold', 
    paddingLeft: 20
  }
});

export class Header extends React.Component {
  render() {
    return (
      <View style={styles.container}> 
        <Image source={require('../../assets/logo.jpg')} style={{width: 40, height: 40}}/>
        <Text style={styles.title}>Sous-Chef</Text>
      </View>
    );
  }
}