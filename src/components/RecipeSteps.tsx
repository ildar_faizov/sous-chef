import React from 'react';
import { View, Modal, ScrollView, TouchableHighlight, Alert } from 'react-native';
import { Step } from '../shared/DataTypes';
import ListBasedStyles from '../shared/ListBasedStyles';
import { Icon, Text } from 'react-native-elements';
import { FlatListRendererArguments } from '../shared/HelperTypes';
import { ColorScheme } from '../shared/ColorScheme';
import { AddStep } from './AddStep';

interface State {
  steps: Step[];
  addStepModalVisible: boolean;
}
export class RecipeSteps extends React.Component<{}, State> {
  constructor(props: any) {
    super(props);

    this.state = {
      steps: [],
      addStepModalVisible: false
    };

    this.addStep = this.addStep.bind(this);
  }

  render() {
    return (
      <View style={ListBasedStyles.container}>
        <ScrollView style={ListBasedStyles.list}>
          {this.state.steps.map((item: Step, index: number) => this.renderStep({item, index}))}
        </ScrollView>
        <Icon
          raised
          reverse
          name="plus"
          type="font-awesome"
          onPress={() => this.setStepModalVisibility(true)}
          color={ColorScheme.c1}
          containerStyle={{alignSelf: 'flex-end'}}/>

        <Modal
          animationType="slide"
          visible={this.state.addStepModalVisible}
          onRequestClose={() => this.setStepModalVisibility(false)}>
          <AddStep 
            onAdd={this.addStep}
            onCancel={() => this.setStepModalVisibility(false)}/>
        </Modal>
      </View>
    );
  }

  renderStep({item, index}: FlatListRendererArguments<Step>): JSX.Element {
    return (
      <TouchableHighlight key={index} onLongPress={() => this.removeStep(index)}>
        <View>
          <Text>Step {index + 1}. {item.time}</Text>
          <Text>{item.description}</Text>
        </View>
      </TouchableHighlight>
    )
  }

  setStepModalVisibility(b: boolean) {
    this.setState({
      addStepModalVisible: b
    });
  }

  addStep(step: Step) {
    this.setState({
      steps: this.state.steps.concat(step),
      addStepModalVisible: false
    })
  }

  removeStep(index: number) {
    Alert.alert(
      'Are you sure?',
      `Remove step #${index + 1} from list of steps?`,
      [
        {text: 'Cancel'},
        {
          text: 'OK',
          onPress: () => this.setState({
              steps: this.state.steps.filter((_item: Step, i: number) => i !== index)
            })
        }
      ]
    );
  }
}