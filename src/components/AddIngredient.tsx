import React from 'react';
import { IngredientRef, Ingredient } from '../shared/DataTypes';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Alert } from 'react-native';
import CommonStyles from '../shared/CommonStyles';
import Autocomplete from 'react-native-autocomplete-input';
import { ingredients } from '../shared/Ingredients';
import { Button } from 'react-native-elements';
import { ColorScheme } from '../shared/ColorScheme';

export interface AddIngredientProps {
  existingIngredients: Set<number>;
  onAdd: (i: IngredientRef) => any;
  onCancel: () => any;
}

interface State {
  title?: string;
  amount?: string;

  hasFocus: boolean;
}

export class AddIngredient extends React.Component<AddIngredientProps, State> {
  constructor(props: AddIngredientProps) {
    super(props);

    this.state = {
      title: '',
      amount: '',
      hasFocus: false
    };
  }

  render() {
    const filter = ingredients.filter((i: Ingredient) => 
      !this.props.existingIngredients.has(i.id) &&
      (!this.state.title || i.title.toLowerCase().startsWith(this.state.title.toLowerCase())));

    const fullMatch = filter.length == 1 && filter[0].title === this.state.title;
    return (
      <View style={{flex: 1, backgroundColor: ColorScheme.c3}}>
        <View style={styles.autocompleteContainer}>
          <Text style={CommonStyles.formControlLabel}>Ingredient</Text>
          <Autocomplete
            defaultValue={this.state.title}
            onChangeText={(value: string) => this.setState({title: value})}
            onFocus={() => this.setState({hasFocus: true})}
            onBlur={() => this.setState({hasFocus: false})}
            hideResults={!this.state.hasFocus || fullMatch}
            data={filter}
            renderItem={ (item: Ingredient) =>
              <TouchableOpacity onPress={() => this.setState({title: item.title})}>
                <Text>{item.title}</Text>
              </TouchableOpacity>
            }
            containerStyle={{backgroundColor: ColorScheme.c3}}
            style={{backgroundColor: ColorScheme.c3, fontSize: 16, height: 40}}
            inputContainerStyle={{borderWidth: 0}}
            listStyle={{backgroundColor: ColorScheme.c3}}
            />
        </View>
        <View style={{marginTop: 70, padding: 5}}>
          <Text style={CommonStyles.formControlLabel}>Amount</Text>
          <TextInput
            value={this.state.amount}
            style={CommonStyles.formControl}
            onChangeText={(value: string) => this.setState({amount: value})}/>

          <View style={{flexDirection: 'row'}}>
            <Button
              title="Cancel"
              onPress={() => this.props.onCancel()}
              containerViewStyle={{flex: 1}}
              backgroundColor={ColorScheme.c1}
              borderRadius={5}
              />
            <Button
              title="Add"
              onPress={() => this.handleAdd()}
              containerViewStyle={{flex: 1}}
              backgroundColor={ColorScheme.c1}
              borderRadius={5}
              />
          </View>
        </View>
      </View>
    );
  }

  handleAdd() {
    if (!this.state.title) {
      Alert.alert(
        'Error',
        'Please enter name of ingredient',
        [{
          text: 'OK'
        }]
      )
    }
    const filter = ingredients.filter((i: Ingredient) => 
      i.title.toLowerCase() === this.state.title.toLowerCase());
    if (filter.length === 1) {
      this.props.onAdd({
        ingredientId: filter[0].id,
        amount: this.state.amount
      });
    } else if (filter.length === 0) {
      // TODO: add
    }
  }
}

const styles = StyleSheet.create({
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1,
    margin: 5
  }
});