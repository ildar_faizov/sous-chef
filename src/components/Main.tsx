import React from 'react';
import { View } from 'react-native';
import { Categories } from './Categories';
import { createStackNavigator } from 'react-navigation';
import { ColorScheme } from '../shared/ColorScheme';
import { Header } from './Header';
import { Recipes } from './Recipes';
import { NewRecipe } from './NewRecipe';

const CategoriesNavigator = createStackNavigator({
  Categories: {
    screen: Categories
  },
  Recipes: {
    screen: Recipes
  },
  NewRecipe: {
    screen: NewRecipe
  }
}, {
  initialRouteName: 'Categories',
  navigationOptions: {
    headerStyle: {
      backgroundColor: ColorScheme.c1
    },
    headerTitleStyle: {
      color: '#fff'
    },
    headerTitle: <Header/>
  }
});

export class Main extends React.Component {

  render() {
    return (
      <View style={{flex: 1, backgroundColor: ColorScheme.c3}}>
        <CategoriesNavigator/>
      </View>
    );
  }
}