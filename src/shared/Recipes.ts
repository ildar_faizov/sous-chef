import { Recipe } from "./DataTypes";

export const recipes : Recipe[] = [
  {
    "id": 0,
    "title": "Spaghetti Pasta Carbonara",
    "categoryId": 3,
    "amount": "4",
    "complexity": 2,
    "time": "25m",
    "ingredients": [
      {
        "ingredientId": 39,
        "amount": null
      },
      {
        "ingredientId": 30,
        "amount": "200g"
      },
      {
        "ingredientId": 45,
        "amount": "1"
      },
      {
        "ingredientId": 28,
        "amount": "3"
      },
      {
        "ingredientId": 29,
        "amount": "200g"
      },
      {
        "ingredientId": 14,
        "amount": "400g"
      }
    ],
    "steps": [
      {
        "description": "Heat water. While the water is coming to a boil, heat the olive oil in a large sauté pan over medium heat. Add the bacon or pancetta and cook slowly until crispy. Add the garlic (if using) and cook another minute, then turn off the heat and put the pancetta and garlic into a large bowl.",
        "time": "10m"
      },
      {
        "description": "In a small bowl, beat the eggs and mix in about half of the cheese.",
        "time": "5m"
      },
      {
        "description": "Once the water has reached a rolling boil, add the dry pasta, and cook, uncovered, at a rolling boil.",
        "time": "3m"
      },
      {
        "description": "When the pasta is al dente (still a little firm, not mushy), use tongs to move it to the bowl with the bacon and garlic. Let it be dripping wet. Reserve some of the pasta water. Move the pasta from the pot to the bowl quickly, as you want the pasta to be hot. It's the heat of the pasta that will heat the eggs sufficiently to create a creamy sauce. Toss everything to combine, allowing the pasta to cool just enough so that it doesn't make the eggs curdle when you mix them in. (That's the tricky part.)",
        "time": "5m"
      },
      {
        "description": "Add the beaten eggs with cheese and toss quickly to combine once more. Add salt to taste. Add some pasta water back to the pasta to keep it from drying out",
        "time": "2m"
      }
    ]
  }
]