interface HasId {
  id: number;
}

interface HasTitle {
  title: string;
}

interface HasImage {
  image?: any; // Base64 or URI
}

export interface Category extends HasId, HasTitle, HasImage {
}

export interface Ingredient extends HasId, HasTitle {
}

export interface Ingredient extends HasId, HasTitle {
}

export interface Step {
  description: string;
  time?: string;
}

export interface IngredientRef {
  ingredientId: number;
  amount?: string;
}

export interface Recipe extends HasId, HasTitle, HasImage {
  categoryId: number;
  amount: string;
  complexity: number;
  time?: string,
  ingredients: IngredientRef[];
  steps: Step[];
}