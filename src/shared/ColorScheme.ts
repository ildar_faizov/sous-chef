// http://color.romanuke.com/tsvetovaya-palitra-3415/
export const ColorScheme = {
  "c1": "#de3410",
  "c2": "#fe8255",
  "c3": "#fed9b1",
  "c4": "#fcd158",
  "c5": "#676f28"
}