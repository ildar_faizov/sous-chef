import { StyleSheet } from "react-native";

export default StyleSheet.create({
  formControlLabel: {
    fontSize: 12
  },
  formControl: {
    fontSize: 16,
    height: 40
  },
  formContainer: {
    flex: 1,
    padding: 5
  }
});