export interface FlatListRendererArguments<T> {
  item: T;
  index: number
}