import { Category } from "./DataTypes";

export const categories : Category[] =
[
  {
    "id": 0,
    "title": "Breakfast",
    "image": require('../../assets/categories/Breakfast.jpg')
  },
  {
    "id": 1,
    "title": "Salads",
    "image": require('../../assets/categories/Salads.jpg')
  },
  {
    "id": 2,
    "title": "Soups",
    "image": require('../../assets/categories/Soups.jpg')
  },
  {
    "id": 3,
    "title": "Main Course",
    "image": require('../../assets/categories/Main_Course.jpg')
  },
  {
    "id": 4,
    "title": "Desserts",
    "image": require('../../assets/categories/Desserts.jpg')
  },
  {
    "id": 5,
    "title": "Drinks",
    "image": require('../../assets/categories/Drinks.jpg')
  }
];