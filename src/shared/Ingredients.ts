import { Ingredient } from "./DataTypes";

export function findIngredientById(id: number): Ingredient | undefined {
  return ingredients.find((i: Ingredient) => i.id === id);
}

export const ingredients: Ingredient[] = [
  {
    "id": 0,
    "title": "Coarse salt"
  },
  {
    "id": 1,
    "title": "Red chili flakes"
  },
  {
    "id": 2,
    "title": "Black peppercorns"
  },
  {
    "id": 3,
    "title": "Coriander"
  },
  {
    "id": 4,
    "title": "Fennel seeds"
  },
  {
    "id": 5,
    "title": "Paprika"
  },
  {
    "id": 6,
    "title": "Oregano"
  },
  {
    "id": 7,
    "title": "Turmeric"
  },
  {
    "id": 8,
    "title": "Whole nutmeg"
  },
  {
    "id": 9,
    "title": "Bay leaves"
  },
  {
    "id": 10,
    "title": "Cayenne pepper"
  },
  {
    "id": 11,
    "title": "Thyme"
  },
  {
    "id": 12,
    "title": "Cinnamon"
  },
  {
    "id": 13,
    "title": "Panko bread crumbs"
  },
  {
    "id": 14,
    "title": "Pasta"
  },
  {
    "id": 15,
    "title": "Couscous"
  },
  {
    "id": 16,
    "title": "Rice"
  },
  {
    "id": 17,
    "title": "All-purpose flour"
  },
  {
    "id": 18,
    "title": "White sugar"
  },
  {
    "id": 19,
    "title": "Brown sugar"
  },
  {
    "id": 20,
    "title": "Powdered sugar"
  },
  {
    "id": 21,
    "title": "Baking powder"
  },
  {
    "id": 22,
    "title": "Active dry yeast"
  },
  {
    "id": 23,
    "title": "Chicken stock"
  },
  {
    "id": 24,
    "title": "Beef stock"
  },
  {
    "id": 25,
    "title": "Milk"
  },
  {
    "id": 26,
    "title": "Butter"
  },
  {
    "id": 27,
    "title": "Heavy cream"
  },
  {
    "id": 28,
    "title": "Eggs"
  },
  {
    "id": 29,
    "title": "Parmesan"
  },
  {
    "id": 30,
    "title": "Bacon"
  },
  {
    "id": 31,
    "title": "Parsley"
  },
  {
    "id": 32,
    "title": "Celery"
  },
  {
    "id": 33,
    "title": "Carrots"
  },
  {
    "id": 34,
    "title": "Lemons"
  },
  {
    "id": 35,
    "title": "Limes"
  },
  {
    "id": 36,
    "title": "Orange juice"
  },
  {
    "id": 37,
    "title": "Ketchup"
  },
  {
    "id": 38,
    "title": "Mayonnaise"
  },
  {
    "id": 39,
    "title": "Extra virgin olive oil"
  },
  {
    "id": 40,
    "title": "vegetable oil"
  },
  {
    "id": 41,
    "title": "Canola/olive oil"
  },
  {
    "id": 42,
    "title": "Vinegar"
  },
  {
    "id": 43,
    "title": "Mustard"
  },
  {
    "id": 44,
    "title": "Honey"
  },
  {
    "id": 45,
    "title": "Garlic"
  },
  {
    "id": 46,
    "title": "Shallots"
  },
  {
    "id": 47,
    "title": "Potatoes — Idaho"
  },
  {
    "id": 48,
    "title": "Onions — yellow"
  },
  {
    "id": 49,
    "title": "Onions — red"
  },
  {
    "id": 50,
    "title": "Tomatoes"
  },
  {
    "id": 51,
    "title": "Tomatoes — diced"
  },
  {
    "id": 52,
    "title": "Tomato sauce"
  },
  {
    "id": 53,
    "title": "Tomato paste"
  },
  {
    "id": 54,
    "title": "Tomatoes — crushed"
  },
  {
    "id": 55,
    "title": "Beans"
  }
]