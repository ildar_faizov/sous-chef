import { StyleSheet } from "react-native";
import { ColorScheme } from "./ColorScheme";

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
    backgroundColor: ColorScheme.c3
  },
  list: {
    flexGrow: 1,
    flexShrink: 1,
    backgroundColor: ColorScheme.c3,
    borderTopColor: ColorScheme.c1,
    borderBottomColor: ColorScheme.c1
  }
});
