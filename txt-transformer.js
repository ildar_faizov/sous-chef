var fs = require('fs');
var filename = process.argv[2];
fs.readFile(filename, 'utf8', function(err, data) {
  if (err)
    throw err;
  var lines = data.split(/\r?\n/);
  var result = [];
  lines.forEach((element, idx) => {
    result.push({
      'id': idx,
      'title': element
    });
  });
  console.log(JSON.stringify(result, null, 2));
});